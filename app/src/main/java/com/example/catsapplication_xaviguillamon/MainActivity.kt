package com.example.catsapplication_xaviguillamon

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.catsapplication_xaviguillamon.ViewModel.CatsViewModel
import com.example.catsapplication_xaviguillamon.ui.Model.CatUIModel
import com.example.catsapplication_xaviguillamon.ui.theme.CatsApplication_XaviGuillamonTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CatApp()
        }
    }
}

@Composable
fun CatApp(catViewModel: CatsViewModel = viewModel()) {
    val uiState by catViewModel.catsUIState.collectAsState()
    uiState?.let {
        CatList(catsList = it)
    }
}

@Composable
fun CatList(catsList: List<CatUIModel>) {
    LazyColumn() {
        this.items(items = catsList, itemContent = { item ->
            CatCard(cat = item)
        })
    }
}

@Composable
fun CatCard(cat: CatUIModel, modifier: Modifier = Modifier) {
    val expanded = remember { mutableStateOf(false) }
    Card(
        border = BorderStroke(5.dp, MaterialTheme.colorScheme.primary),
    ) {
        Column(
            modifier = Modifier
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)

            ) {
                Image(
                    painter = rememberAsyncImagePainter(R.drawable.corgi),
                    contentDescription = "ShowImage",
                    modifier = Modifier
                        .size(100.dp)
                        .padding(8.dp)
                        .clip(CutCornerShape(20))
                        .border(5.dp, MaterialTheme.colorScheme.primary, shape = CutCornerShape(20)),
                )
                Spacer(Modifier.padding(20.dp))
                Text(
                    text = cat.name,
                    modifier = Modifier.padding(16.dp),
                    style = TextStyle(
                        fontSize = 24.sp
                    ),
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center,
                )
                Spacer(Modifier.padding(80.dp))
                DogItemButton(
                    expanded = expanded.value,
                    onClick = {
                        expanded.value = !expanded.value
                    }
                )
            }
            if (expanded.value) {
                Description(cat)
            }
        }
    }
}

@Composable
private fun DogItemButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector =
            if (expanded)
                Icons.Filled.Close
            else
                Icons.Filled.Info,
            tint = MaterialTheme.colorScheme.secondary,
            contentDescription = "Boton desplegable",
            modifier = Modifier.requiredWidth(50.dp)
        )

    }
}

@Composable
private fun Description(cat: CatUIModel, modifier: Modifier = Modifier) {
    val context = LocalContext.current
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier.padding(
                start = 16.dp,
                top = 8.dp,
                bottom = 16.dp,
                end = 16.dp
            )
        ) {
            Text(
                text = "Description: ",
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
            )
            Text(
                text = cat.description,
                fontSize = 20.sp,
                modifier = Modifier
                    .padding(8.dp),
                overflow = TextOverflow.Ellipsis,
                maxLines = 2,
            )
        }
    }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxSize()
            .padding(4.dp)
    ) {
        Spacer(Modifier.padding(10.dp))

        val context = LocalContext.current
        TextButton(onClick = {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("cat", cat)
            context.startActivity(intent)
        }) {
            Text("See more")
        }
    }
}