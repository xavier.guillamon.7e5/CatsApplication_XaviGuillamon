package com.example.catsapplication_xaviguillamon.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catsapplication_xaviguillamon.Data.APIService.CatsApi
import com.example.catsapplication_xaviguillamon.ui.Model.CatUIModel
import com.example.catsapplication_xaviguillamon.ui.Model.Mappers.CatsDtoUIModelMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch


class CatsViewModel : ViewModel() {

    private val _catsUIState = MutableStateFlow<List<CatUIModel>?>(null)
    val catsUIState: StateFlow<List<CatUIModel>?> = _catsUIState.asStateFlow()

    var mapper = CatsDtoUIModelMapper()

    init {
        getCats()
    }

    fun getCats() {
        viewModelScope.launch {
            val breedListDto = CatsApi.retrofitService.getBreed()
            //val imageListDto = breedListDto.flatMap { CatsApi.retrofitService.getPhotos(it.id) }
            _catsUIState.value = mapper.map(breedListDto, emptyList())
        }
    }
}
