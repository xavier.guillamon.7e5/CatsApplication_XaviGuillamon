package com.example.catsapplication_xaviguillamon.Data.APIService

import com.example.catsapplication_xaviguillamon.Data.APIService.Model.BreedDto
import com.example.catsapplication_xaviguillamon.Data.APIService.Model.CatImageDto
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

private const val BASE_URL = "https://api.thecatapi.com"
private val retrofit = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(BASE_URL)
    .build()

interface CatApiService {
    @GET("./v1/breeds")
    suspend fun getBreed() : List<BreedDto>
    @GET("./v1/images/search")
    @Headers("x-api-key: live_W2zwMFIqo4Vitu6FlbrJlMhxWL4E1u6mfSLAvgpfTg2YJMsEFmDLtn5JHHZ2NpKk")
    suspend fun getPhotos(@Query("breed_ids")id:String) : List<CatImageDto>
}

object CatsApi {
    val retrofitService : CatApiService by lazy {
        retrofit.create(CatApiService::class.java)
    }
}