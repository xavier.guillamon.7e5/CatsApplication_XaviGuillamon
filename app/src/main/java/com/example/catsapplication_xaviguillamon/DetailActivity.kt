package com.example.catsapplication_xaviguillamon


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.catsapplication_xaviguillamon.ui.Model.CatUIModel
import androidx.compose.ui.tooling.preview.Preview as Preview1

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val cat = intent.getParcelableExtra<CatUIModel>("cat")

        setContent {
            CatDetail(cat!!)
        }
    }
}

@Composable
fun CatDetail(cat: CatUIModel) {
    Box {
        Image(
            painterResource(id = R.drawable.background), contentDescription = null,
            modifier = Modifier
                .fillMaxSize()
                .alpha(0.5f),
            contentScale = ContentScale.FillHeight

        )
    }
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth(),

        ) {
        Image(
            painter = rememberAsyncImagePainter(R.drawable.corgi),
            contentDescription = "image",
            modifier = Modifier
                .size(350.dp)
                .padding(8.dp)
                .clip(CutCornerShape(20))
        )
        Text(
            text = cat.name,
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.ExtraBold,
            style = MaterialTheme.typography.headlineMedium,
            modifier = Modifier.padding(16.dp),
            fontSize = 30.sp
        )
        Text(
            text = cat.description,
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier
                .padding(14.dp)
                .background(color = Color.White, shape = RoundedCornerShape(10.dp)),
            fontSize = 20.sp
        )
        if (cat.codCountry != null) {
            Text(
                text = cat.codCountry,
                style = MaterialTheme.typography.bodyMedium,
                modifier = Modifier.padding(16.dp)
            )
        }
        Text(
            text = cat.temperamnet,
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier
                .padding(24.dp)
                .background(color = Color.White, shape = RoundedCornerShape(10.dp)),
            fontWeight = FontWeight.ExtraBold
        )
        if (cat.wikiURL != null) {
            Text(
                text = cat.wikiURL,
                style = MaterialTheme.typography.bodyMedium,
                modifier = Modifier.padding(16.dp)
            )
        }

    }
}