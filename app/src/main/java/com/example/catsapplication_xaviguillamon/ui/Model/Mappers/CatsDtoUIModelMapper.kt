package com.example.catsapplication_xaviguillamon.ui.Model.Mappers

import com.example.catsapplication_xaviguillamon.Data.APIService.Model.BreedDto
import com.example.catsapplication_xaviguillamon.Data.APIService.Model.CatImageDto
import com.example.catsapplication_xaviguillamon.ui.Model.CatUIModel

class CatsDtoUIModelMapper {
    fun map(listBreeds : List<BreedDto>, listImages : List<CatImageDto>): List<CatUIModel>{
        return listBreeds.mapIndexed { index, breed ->
            CatUIModel(
                url = "",
                name = breed.name,
                description = breed.description,
                codCountry = breed.countryCode,
                temperamnet = breed.temperament,
                wikiURL = breed.wikipediaUrl
            )
        }
        return mutableListOf<CatUIModel>().apply {
            listBreeds.forEachIndexed{
                    index, breed ->
                add(CatUIModel(
                    url = listImages[index].url,
                    name = breed.name,
                    description = breed.description,
                    codCountry = breed.countryCode,
                    temperamnet = breed.temperament,
                    wikiURL = breed.wikipediaUrl
                ))
            }
        }
    }
}